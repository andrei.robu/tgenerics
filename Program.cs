﻿using System;
using System.Collections.Generic;

namespace Tgenerics
{
    class Program : TGenerics<int>
    {
        static void Main(string[] args)
        {

            // 1)
            int[] a = { 1, 2, 3 };
            int[] b = Backwrd(ref a);
            foreach (var nr in b)
            {
                Console.Write(nr + " ");
            }
            Console.Write("\n\n\n");
            // 2)
            var anObject = new aClass<Program, Program>();
            anObject.Afisare();
            // 3)
            var c = new HashSet<int>();
            // 4)
            var Customers = new List<Customer>();
            Customer customer1 = new Customer("Alex", 25);
            customer1.Orders.Add(new Comanda(1, "Lampa birou", "21.12.2021\n"));
            customer1.Orders.Add(new Comanda(2, "Scaun birou", "21.12.2021\n"));
            customer1.Orders.Add(new Comanda(3, "Set 5 pixuri", "21.12.2021\n"));
            Customers.Add(customer1);
            Customer customer2 = new Customer("Valentin", 25);
            customer2.Orders.Add(new Comanda(4, "Hanorac", "21.12.2021\n"));
            customer2.Orders.Add(new Comanda(5, "Tricou polo", "21.12.2021\n"));
            customer2.Orders.Add(new Comanda(6, "Pantaloni scurti cargo", "21.12.2021\n"));
            Customers.Add(customer2);
            Customer customer3 = new Customer("Roxana", 25);
            customer3.Orders.Add(new Comanda(7, "Cornulete 7days", "21.12.2021\n"));
            customer3.Orders.Add(new Comanda(8, "Cafea", "21.12.2021\n"));
            Customers.Add(customer3);
            var colec = new SortedList<string, List<Comanda>>();
            foreach (var VARIABLE in Customers)
            {
                colec.Add(VARIABLE.Name, VARIABLE.Orders);
            }
            Console.Write("\n\n");
            foreach (var VARIABLE in colec)
            {
                Console.Write(VARIABLE.Key + ":\n");
                foreach (var VARIABLE1 in VARIABLE.Value)
                {
                    Console.Write(VARIABLE1.toString() + " ");
                }
                Console.Write("\n");
            }
            // 5)
            var repository = new LinkedList<Customer>();
            Customer customer4 = new Customer();
            customer4 = customer4.Create("Maria", "25", null);
            repository.AddFirst(customer4);
        }
        static T[] Backwrd<T>(ref T[] inp)
        {
            int nr = inp.Length;
            T[] outp = new T[nr];
            int j = 0;
            for (int i = nr - 1; i >= 0; i--)
            {
                outp[j] = inp[i];
                j++;
            }
            return outp;
        }
    }

    public interface TGenerics<T>
    {

    }

    public class Customer : Crud<Customer, string>
    {
        public string Name;
        public int Age;
        public List<Comanda> Orders;

        public Customer(string name, int age)
        {
            Name = name;
            Age = age;
            Orders = new List<Comanda>();
        }

        public Customer()
        {

        }

        public Customer Create(string value, string value1, string value2)
        {
            return new Customer(value, Convert.ToInt32(value1));
        }

        public void Delete(ref Customer item)
        {
            item = null;
        }

        public void Read()
        {
            Console.WriteLine(this.Name + " " + this.Age);
            foreach (var VARIABLE in Orders)
            {
                VARIABLE.Read();
            }
        }

        public void Update(string value, string value1, string value2)
        {
            this.Name = value;
            this.Age = Convert.ToInt32(value1);

        }
    }

    public class Comanda : Crud<Comanda, string>
    {
        public int Number;
        public string Title;
        public string Date;

        public Comanda(int number, string title, string date)
        {
            Number = number;
            Title = title;
            Date = date;
        }

        public Comanda Create(string value, string value1, string value2)
        {
            var ord = new Comanda(Convert.ToInt32(value), value1, value2);
            return ord;
        }

        public void Delete(ref Comanda item)
        {
            item = null;
        }

        public void Read()
        {
            Console.WriteLine(this.toString());
        }

        public string toString()
        {
            return Number.ToString() + " " + Title + " " + Date;
        }

        public void Update(string value, string value1, string value2)
        {
            this.Number = Convert.ToInt32(value);
            this.Date = value2;
            this.Title = value1;
        }
    }

    public interface Crud<T, T1>
    {
        T Create(T1 value, T1 value1, T1 value2);
        void Read();
        void Update(T1 value, T1 value1, T1 value2);
        void Delete(ref T item);
    }
    /////////////////////
    public class aClass<T1, T2>
    where T1 : class, new()
    where T2 : TGenerics<int>
    {
        public void Afisare()
        {
            Console.Write(typeof(T1) + " " + typeof(T2));
        }
    }
    /////////////////////
}
